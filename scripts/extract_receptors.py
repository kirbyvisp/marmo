#!/usr/bin/env python3

__author__ = 'David Koppstein'

# script for extracting receptors from
# currently works on ENSEMBL human release only

# requirements: gtf2bed, bedtools in PATH

import argparse
from subprocess import call
import os
import sys

universal_regex = '"((IG|TR)_[VCJ]_(pseudo)?gene|TRBV25)"'
bcr_regex = '"IG_[VCJ]_(pseudo)?gene"'
tcr_regex = '"(TR_[VCJ]_(pseudo)?gene|TRBV25)"' # apparently TRBV25OR9 is a special case


chains = {'TRA': [tcr_regex, '-v "(T(R|r)(D|d)[cCjJvV])"', '"^(chr)?14"'], # 'T(R|r)(A|a)[cCjJvV]',
          'TRB': [tcr_regex, '-v "(TRG[CJV]|Tcrg)"', '"^(chr)?(6|7|9)"'], # human or mouse 'T(R|r)(B|b)[cCjJvV]|ENST00000634383',
          'TRD': [tcr_regex, '"(T(R|r)(D|d)[cCjJvV])"', '"^(chr)?14"'], # ,
          'TRG': [tcr_regex, '"(TRG[CJV]|Tcrg)"', '"^(chr)?(7|13)"'], # 13 = mouse
          'IGH': [bcr_regex, '"^(chr)?(12|14)"'], # 'I(G|g)(H|h)[vVjJaAdDeEgGmM]',
          'IGK': [bcr_regex, '"^(chr)?(2|6)"'], # 'I(G|g)(K|k)[cCjJvV]',
          'IGL': [bcr_regex, '"^(chr)?(16|22)"']} # 'I(G|g)(L|l)[cCvVjJ]',

command = ('awk \'{{ if ($0 ~ "transcript_id") print $0; else print $0" transcript_id \\"\\";"; }}\' {infile} | '
           'grep -v PATCH | '
           '{egrep} | '
           'gtf2bed - | '
           'sort -k1,1 -k2,2n | '
           'bedtools merge -i - '
           '> {outfile}')

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', type=str,
                        required=True)
    parser.add_argument('-c', '--chain', type=str, required=True)
    parser.add_argument('-o', '--outfile', type=str,
                        required=True)
    opts = parser.parse_args()
    egrep = ' | '.join(['egrep {}'.format(regex) for regex in chains[opts.chain]])
    cmd = command.format(infile=opts.infile,
                         egrep=egrep,
                         outfile=opts.outfile)
    call(cmd, shell=True, executable='/bin/bash')


if __name__ == '__main__':
    main(sys.argv[1:])
