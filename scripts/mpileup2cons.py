# mpileup2cons - uses the output of mpileup and bcftools to modify nucleotides
#  in a reference sequence where the majority of reads differ from the reference.
#
# This has been modified from the utility in vdjpuzzle2 by updating the code for
#  Python 3 and implementing it as a function that modifies the reference in
#  memory rather than a command line utility that reads from a file and stdin
#  and writes to a file. It has also been updated to use the biopython library.

import csv
import sys
from Bio.SeqRecord import SeqRecord

def mpileup2cons(changes_csv_iter, seq_record, log_file):
    """
    Uses the output of mpileup and bcftools view -cg to modify nucleotides
    in a reference sequence where the majority of reads differ from the reference.

    Args:
        changes_csv_iter: an iterator over the lines output by mpileup and bcftools view -cg
        seq_record: a SeqRecord object containing the original sequence
        log_file: a file object to write log messages to

    Returns:
        a SeqRecord object containing the modified sequence, with the same name
        etc. as seq_record
    """

    ## Parse the output of mpileup and bcftools view -cg to obtain positions
    ## where majority nucleotide != original reference.
    all_changes = []
    for line in csv.reader(changes_csv_iter,delimiter='\t'):
        if line[0][0] == "#": # skip comments
            continue
        elif line[4] not in ["A","C","G","T"]: # skip if not recognised
            continue
        else:
            # Determine frequency of variant
            info = line[7].split(";")
            # frequencies is [ref-forward , ref-reverse , alt-forward , alt-reverse]
            frequencies = list(map(int, list(filter(lambda x: 'DP4=' in x, info))[0][4:].split(',')))
            ref_freq = frequencies[0] + frequencies[1]
            var_freq = frequencies[2] + frequencies[3]

            if var_freq > ref_freq:
                change = [line[1],line[3],line[4]] # position,reference,variant
                all_changes.append(change)

    original_seq = seq_record.seq
    new_seq = original_seq.tomutable()
    log_file.write("Processing sequence {0}\n".format(seq_record.description))
    for entry in all_changes:
        pos,old_nuc,new_nuc = entry
        assert original_seq[int(pos)-1] == old_nuc, "The positions of the required changes are not aligned with the original reference sequence."
        log_file.write("Changing position {0} from nucleotide {1} to {2}.\n".format(pos,old_nuc,new_nuc))
        new_seq[int(pos)-1] = new_nuc # replace nucleotides with changes identified from mpileup and bcftools

    # Keep the old record name, description etc. but replace the sequence
    return seq_record[0:0] + new_seq
