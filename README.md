# README #

An integrated and configurable Snakemake pipeline for single-cell analysis of SMART-Seq2 data from start to finish. Currently supports scRNA-seq and immune repertoire reconstruction.

# Quick start guide

1. Install conda and put the install directory somewhere on NFS. Be sure that `conda` is in your PATH.

2. On some clusters, `set -u` is applied by default and thus unbound variables cause scripts to crash. This can cause a problem for conda but can be fixed by copying over the files in the `conda` directory to their respective locations in the conda base installation.

3. On some SGE clusters, there is an issue with formatting wildcards for the qsub command.
This can be solved by modifying `utils.py` in the miniconda environment with the one in the `snakemake/` directory, i.e. `miniconda/envs/lib/python3.6/site-packages/snakemake` See issue #2.

4. Create the conda environment for running the pipeline.

```
conda create -n indexsort --file environment-explicit.txt
```

5. Edit `config/defaults.yaml` and `config/cluster.yaml` to suit your local system. In particular, be sure to
set all the parameters preceded by two underscores, i.e. `__cluster_job_cmd__`.

7. Symlink bin/marmo somewhere into your PATH, e.g. `~/bin`.

8. Run `marmo -i /path/to/data`

# Cluster configuration

Some clusters have different parameters that are required, or do not allow jobs to submit further jobs. For example, on the Garvan cluster, you can use 

`marmo -i /path/to/data --cluster-config /path/to/marmo/config/garvan.yaml --submit-jobs-only`

However, in the long run we should consider using [this](https://github.com/snakemake-profiles). 

# Rule DAG

As of March 14, 2018:

![Scheme](images/ruledag.png)
