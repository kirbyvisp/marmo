import yaml
import os
from os.path import join, basename, dirname, relpath
import collections
from subprocess import check_output
import sys
from Bio import SeqIO
import pandas as pd

# Workaround to allow python modules to be imported other folders in the marmo
#  source (eg. from the scripts folder)
sys.path.append(srcdir('..'))

# update the config variable
def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.Mapping):
            r = update(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d

def pandas_merge(input_files,
                 output,
                 add_columns=None,
                 debug=False,
                 deduplicate_cols=None,
                 overwrite_cols=False,
                 comment=None):
    with open(str(output), 'a') as outh:
        for i, infile in enumerate(input_files):
            df = pd.read_table(infile, index_col=None, comment=comment)
            if add_columns:
                assert type(add_columns) is dict
                for k, col_values in add_columns.items():
                    # check that the length of items matches the input files
                    assert len(col_values) == len(input_files)
                    if k not in df.columns or overwrite_cols:
                        df[k] = col_values[i]
            if debug:
                print(df, file=sys.stderr)
            if deduplicate_cols:
                # does this work?
                assert type(deduplicate_cols) is list and all(type(x) is str for x in deduplicate_cols)
                for col in deduplicate_cols:
                    increment_by = dfs[0][col].max() + 1
                    for df in dfs[1:]:
                        df[col] += increment_by
                        increment_by += df[col].max() + 1
            if i == 0:
                # remember column order
                cols_ord = df.columns.tolist()
            # only write the header if it's the first time around
            df[cols_ord].to_csv(outh, index=False, header=(i == 0), sep='\t')


def merge_input(input, output,
                merge_headers=False,
                deduplicate_cols=None,
                add_columns=None,
                overwrite_cols=False,
                debug=False,
                comment=None):
    # Utility function for concatenating input files into a single output file;
    # if only one file exists, as input, symlink it to output.
    # `input` is the input object from Snakemake.
    # If `merge_headers` is specified, will omit the headers from the second to last files
    # while keeping the first.

    # If `deduplicate_cols` is specified, will increment each subsequent DataFrame's column by
    # the maximum value of the previous DataFrame + 1.
    # Currently only works for integers and is used for deduplicating CLONE.

    # Optionally add columns to the input files, iff merge_headers is specified.
    # add_columns is a dictionary whose key `k` is the column name to add, and the value is
    # a list of strings whose length matches the number of input files and the ith member of
    # the list will be added to column k for input file i.

    # e.g. {'POOL': ['160406_1', ''160406_2', '160406_3', '160406_4']} will add that value
    # to column POOL for each input file.

    # remove output file if it exists since we're using append
    if os.path.exists(str(output)): # TODO check if it's a symlink
        os.unlink(str(output))

    input_files = str(input).split()
    # if just one sample, cat the input to the output
    if len(input_files) == 1 and add_columns is None:
        inputstr = str(input)
        shell('cat {inputstr} > "{output}"')
        #relative_symlink(input, output)
        return

    if add_columns and not merge_headers:
        raise Exception("Can't add columns without merging headers.")

    # if we're merging headers, feed it into pandas
    elif merge_headers:
        pandas_merge(input_files,
                     output,
                     deduplicate_cols=deduplicate_cols,
                     add_columns=add_columns,
                     overwrite_cols=overwrite_cols,
                     debug=debug,
                     comment=comment)
    else:
        inputs = ' '.join('"{}"'.format(i) for i in str(input).split())
        shell('cat {inputs} > "{output}"')


_defaults = yaml.load(open(srcdir('../config/defaults.yaml')))

if 'config' not in globals():
    config = _defaults
else:
    config = update(_defaults, config)

# global variables
CONDA = 'set +u; source activate {conda_env} &&'.format(conda_env=config['conda_env'])
fastq_dir = config['data_dir']
samples, = glob_wildcards(join(fastq_dir, '{{sample}}{}'.format(config['fastq_suffix']['read_one'])))

print('Starting Snakemake run on the following samples: {}'.format(samples), file=sys.stderr)

def lock_wrapper(cmd, **kwargs):
    '''
    Create a lockfile in the same directory as an output file.
    If it already exists, raise an Error, otherwise execute cmd and remove the
    lockfile. Call with `lock_wrapper(cmd, **locals())` to access
    {input} {output} etc.
    '''
    lockfile = join(dirname(str(kwargs['output']).split()[0]),
                    '.snakelock.' + basename(str(kwargs['output'])))
    if os.path.exists(lockfile):
        raise Exception('Error: downloading assembly report '
                        'in progress. Lockfile {} present. '
                        'Please delete it and try again. '
                        .format(lockfile))
    shell('touch {lockfile}')
    try:
        shell('chmod 777 {lockfile}') # so others can delete
        kwargs.update(globals()) # get config etc.
        shell(cmd.format(**kwargs))
        shell('chmod 755 {output}'.format(**kwargs))
    finally: # remove the lockfile if the command fails for some reason
        shell('rm -f {lockfile}')

# relevant chains
if config['receptor_type'] == 'B':
    relevant_chains = ['IGH', 'IGK', 'IGL']
elif config['receptor_type'] == 'T':
    relevant_chains = ['TRA', 'TRB', 'TRD', 'TRG']
else:
    relevant_chains = None

# SHARED genome
rule get_genome:
    output: join(config['db_dir'], config['genome']['local_path'])
    run:
        lock_wrapper('wget -O - {config[genome][remote]} | '
                     'gunzip - > {output} && chmod 775 {output}', **locals())

# SHARED gtf annotation
rule get_gtf_annotation:
    output: join(config['db_dir'], config['gtf_annotation']['local_path'])
    run:
        lock_wrapper('wget -O - {config[gtf_annotation][remote]} | '
                     'gunzip - > {output} && chmod 775 {output}', **locals())


# e.g. /short/va1/shared/db/annotation/human/gencode/receptors/
receptor_dir = join(config['db_dir'],
                    dirname(config['gtf_annotation']['local_path']),
                    config['extract_receptors']['subdir'])


extract_receptors = srcdir('../scripts/extract_receptors.py')

# gtf2bed trick from https://www.biostars.org/p/206342/
rule extract_receptors:
    input: join(config['db_dir'], config['gtf_annotation']['local_path'])
    output: 'db/{chain}.bed'
    shell:
        '{CONDA} python {extract_receptors} '
        '-i {input} '
        '-c {wildcards.chain} '
        '-o {output}'


rule get_append_genome:
    output: [append_file['local_path'] for append_file in config['append_genome']]
    run:
        remotes = [append_file['remote'] for append_file in config['append_genome']]
        for i, remote in enumerate(remotes):
            if not remote and not exists(output[i]):
                raise Exception('Error: no remote specified for remote {} and '
                                'file {} does not exist.'
                                .format(remote, output[i]))
            cmd = 'wget -O - {remote} '
            if remote.endswith('gz'):
                cmd += '| gunzip - '
            cmd += '> {output[i]} && chmod 775 {output[i]}'
            print(cmd, file=sys.stderr)
            print(locals(), file=sys.stderr)
            lock_wrapper(cmd, **locals())
            if 'ebv.' in remote: # reformat EBV file in place for appending
                lock_wrapper('sed -i "s/chr1/ebv/g" {output[i]} && chmod 775 {output[i]}', **locals())


rule get_append_gtf:
    output: [append_file['local_path'] for append_file in config['append_gtf'] \
             if append_file['remote']]
    run:
        remotes = [append_file['remote'] for append_file in config['append_gtf']]
        for i, remote in enumerate(remotes):
            if not remote and not exists(output[i]):
                raise Exception('Error: no remote specified for remote {} and '
                                'file {} does not exist.'
                                .format(remote, output[i]))
            cmd = 'wget -O - {remote} '
            if remote.endswith('gz'):
                cmd += '| gunzip - '
            cmd += '> {output[i]} && chmod 775 {output[i]}'
            lock_wrapper(cmd, **locals())
            if 'ebv.' in remote: # reformat EBV file in place for appending
                lock_wrapper('sed -i "s/chr1/ebv/g" {output[i]} && chmod 775 {output[i]}', **locals())


rule get_ercc:
    output:
        zipfile=temp('tmp/ERCC92.zip'),
        gtf=join(config['db_dir'], config['ercc']['local_gtf']),
        fasta=join(config['db_dir'], config['ercc']['local_fasta'])
    run:
        lock_wrapper('wget -O - {config[ercc][remote]} > {output.zipfile} && '
                     'unzip -d tmp {output.zipfile} && '
                     'mv tmp/ERCC92.gtf {output.gtf} && '
                     'mv tmp/ERCC92.fa {output.fasta} && '
                     'chmod 775 {output}',
                     **locals())


rule append_genome:
    input:
        genome=rules.get_genome.output,
        ercc=rules.get_ercc.output.fasta if config['include_ercc'] else '',
        appended=[str(x) for x in rules.get_append_genome.output]
    output: temp('db/genomes/genome_appended.fa')
    shell: 'cat {input} > {output}'


rule append_gtf:
    input:
        gtf=rules.get_gtf_annotation.output,
        ercc=rules.get_ercc.output.gtf if config['include_ercc'] else '',
        appended=[str(x) for x in rules.get_append_gtf.output]
    output: 'db/annotation/annotation_appended.gtf'
    shell: 'cat {input} > {output}'


def _starindex_genome(wc):
    if config['append_genome'] or config['include_ercc']:
        return str(rules.append_genome.output)
    else:
        return str(rules.get_genome.output)


def _starindex_annotation(wc):
    if config['append_gtf'] or config['include_ercc']:
        return str(rules.append_gtf.output)
    else:
        return str(rules.get_gtf_annotation.output)

# add unique starindex identifiers to output path for each appended file
starindex_suffix = []

# add ERCC to suffix
if config['include_ercc']:
    starindex_suffix.append('_appended-ercc')

# add additional appended files
for addl_index in config['append_genome']:
    starindex_suffix.append('_appended-{}'
                            .format(basename(addl_index['local_path'])
                                             .split('.')[0]))

# join it all together
# e.g. appended-ebv_appended-hpv if ebv.fa and hpv.fa are appended
starindex_suffix = ''.join(starindex_suffix)

# directory for starindex
# e.g. /short/va1/shared/db/index/human/star/overhang-149_appended-ebv/.starsuccess
starindex_dir = '{}_overhang-{}{}'.format(join(config['db_dir'], config['starindex']['genome_dir']),
                                          config['starindex']['overhang'],
                                          starindex_suffix)

# directory for rsem
rsem_dir = join(config['db_dir'], config['rsem_reference']['genome_dir']) + starindex_suffix
starindex_output = join(starindex_dir, '.starsuccess')

# SHARED star index
if config['rebuild_star_index']:
    rule starindex:
        input: # raw data from e.g. hg38
            genome=_starindex_genome,
            annotation=_starindex_annotation
        output: starindex_output
        threads: config['threads']
        run:
            outdir = dirname(str(output))
            lock_wrapper('{CONDA} STAR '
                         '--runThreadN {threads} '
                         '--runMode genomeGenerate '
                         '--genomeDir {outdir} '
                         '--genomeFastaFiles {input.genome} '
                         '--sjdbGTFfile {input.annotation} '
                         '--sjdbOverhang {config[starindex][overhang]} && '
                         'mv Log.out log/run/{config[run_number]}/rule-starindex.log && '
                         'touch {output}',
                         **locals())

def raw_data(read='read_one'):
    return '{}/{{sample}}{}'.format(fastq_dir, config['fastq_suffix'][read])

rule trimmomatic_pe:
    input:
        # e.g. data/{sample}_1.fastq.gz
        r1=raw_data(read='read_one'),
        r2=raw_data(read='read_two')
    output:
        r1=temp('intermediate/preprocess/{sample}_paired_1.fastq.gz'),
        r2=temp('intermediate/preprocess/{sample}_paired_2.fastq.gz'),
        r1_unpaired=temp('intermediate/preprocess/{sample}_1_unpaired.fastq.gz'),
        r2_unpaired=temp('intermediate/preprocess/{sample}_2_unpaired.fastq.gz')
    shell: # default trimmomatic values from http://www.usadellab.org/cms/?page=trimmomatic
        '{CONDA} trimmomatic PE '
        '-phred33 {input} '
        '{output.r1} {output.r1_unpaired} '
        '{output.r2} {output.r2_unpaired} '
        'ILLUMINACLIP:$CONDA_PREFIX/share/trimmomatic/adapters/NexteraPE-PE.fa:2:30:10 ' # :<seed mismatches>:<palindrome clip threshold>:<simple clip threshold>
        'LEADING:3 ' # minimum quality required to keep a base at 5' end
        'TRAILING:3 ' # minimum quality required to keep a base at 3' end
        'SLIDINGWINDOW:4:15 ' # <windowSize>:<requiredQuality>
        'MINLEN:36'

def _star_input(wc, read='read_one'):
    if config['trim']:
        if read == 'read_one':
            return rules.trimmomatic_pe.output.r1
        else:
            return rules.trimmomatic_pe.output.r2
    else:
        return raw_data(read=read).format(sample=wc.sample)

rule star:
    input:
        read_one=_star_input,
        read_two=lambda wc: _star_input(wc, read='read_two'),
        starindex=starindex_output
    output:
        genome=temp("intermediate/rnaseq/1_mapped/{sample}/{sample}_quantmode-genome_aligned.bam"),
        transcriptome=temp("intermediate/rnaseq/1_mapped/{sample}/{sample}_quantmode-transcriptome_aligned.bam")
    params:
        outdir="intermediate/rnaseq/1_mapped/{sample}"
    threads: config['threads']
    log:
        'intermediate/rnaseq/1_mapped/{sample}/Log.final.out',
        'intermediate/rnaseq/1_mapped/{sample}/Log.progress.out',
        'intermediate/rnaseq/1_mapped/{sample}/Log.std.out',
        'intermediate/rnaseq/1_mapped/{sample}/SJ.out.tab'
    run:
        genome_dir = dirname(str(input.starindex))
        shell('{CONDA} STAR '
              '--genomeDir {genome_dir} '
              '--runThreadN {threads} '
              #'--alignIntronMax 100000 ' # VA's pipeline/ENCODE
              #'--outFilterMultimapNmax 20 ' # max number of multiple alignments per read
              #'--alignSJDBoverhangMin 1 ' # minimum overhang for annotated spliced alignments
              '--readFilesIn {input.read_one} {input.read_two} '
              '--readFilesCommand zcat ' # unzip the trimmomatic output
              '--sjdbOverhang {config[starindex][overhang]} ' # TODO determine this dynamically c.f. genome indexing and wildcards
              '--outSAMtype BAM SortedByCoordinate ' # so don't have to use samtools
              '--outSAMstrandField intronMotif ' # for cufflinks/cuffdiff?
              '--outFileNamePrefix {params.outdir}/ '
              '--outFilterType BySJout ' # clean up rare junctions c.f. http://seqanswers.com/forums/showthread.php?t=30905
              '--outSAMunmapped Within ' # output unmapped reads within the main SAM file
              '--outFilterIntronMotifs RemoveNoncanonicalUnannotated ' #  keep annotated non-canonical junctions but remove the rest
              '--quantMode TranscriptomeSAM '
              # '--chimSegmentMin 25 ' # let's not do chimeric alignments just yet
              # '--chimJunctionOverhangMin 25 '
              '--outStd BAM_SortedByCoordinate ' # have to use with outSAMtype BAM sortedbycoordinate
              '> {output.genome}')
        shell('mv {params.outdir}/Aligned.toTranscriptome.out.bam {output.transcriptome}')


rule samtools_sort:
    input: str(rules.star.output.genome).replace('genome', '{quantmode}')
    output:
        bam='intermediate/rnaseq/1_mapped/{sample}/{sample}_quantmode-{quantmode}_aligned.sorted.bam'
#        index='intermediate/rnaseq/1_mapped/{sample}/{sample}_quantmode-{quantmode}_aligned.sorted.bam.bai'
    params:
        sort_prefix='intermediate/rnaseq/1_mapped/{sample}/{sample}_quantmode-genome_aligned.sorted',
        mem=24
    threads: config['threads']
    shell:
        '''
        {CONDA} samtools sort -@ {threads} -m {params.mem}G -o {output.bam} {input}
        '''


# for MAX_FILE_HANDLES_FOR_READ_ENDS_MAP, see picard documentation
max_file_handles = int(int(check_output('ulimit -n', shell=True, universal_newlines=True)) / 2)

rule remove_dup:
    input: rules.samtools_sort.output.bam
    output:
        bam=temp('intermediate/rnaseq/2_picard_markdup/{sample}_quantmode-{quantmode}_markdup.bam'),
        metrics='intermediate/rnaseq/2_picard_markdup/{sample}_quantmode-{quantmode}_markdup_metrics.txt'
    threads: config['threads']
    shell:
        'mkdir -p tmp/{wildcards.sample}; '
        '{CONDA} picard -Xmx4g MarkDuplicates '
        '-Djava.io.tmpdir=`pwd`/tmp/{wildcards.sample} '
        'I={input} '
        'O={output.bam} '
        'M={output.metrics} '
        'MAX_FILE_HANDLES_FOR_READ_ENDS_MAP={max_file_handles} '
        'REMOVE_DUPLICATES=true '
        'TMP_DIR=`pwd`/tmp/{wildcards.sample} '

def _bam_input(wc, bam_type='genome'):
    if config['remove_dup']:
        bam = rules.remove_dup.output.bam
    else:
        bam = rules.samtools_sort.output.bam
    return str(bam).format(quantmode=bam_type, sample=wc.sample)

rsem_prefix = join(rsem_dir, 'RSEM')
rsem_reference = join(rsem_dir, '.rsemsuccess')

if config['rebuild_rsem_reference']:
    rule rsem_reference:
        input:
            genome=_starindex_genome,
            annotation=_starindex_annotation
        output: rsem_reference
        threads: config['threads']
        params:
            prefix=rsem_prefix
        shell:
            '{CONDA} rsem-prepare-reference '
            '--gtf {input.annotation} '
            '-p {threads} '
            '{input.genome} '
            '{params.prefix}; '
            'touch {output}'


# https://github.com/ENCODE-DCC/long-rna-seq-pipeline/blob/master/DAC/STAR_RSEM.sh
rule rsem:
    input:
        bam=lambda wc: _bam_input(wc, bam_type='transcriptome'),
        ref=rsem_reference
    output:
        final='intermediate/rnaseq/rsem/{sample}_rsem.genes.results',
        bam=temp('intermediate/rnaseq/rsem/{sample}_rsem.bam')
    threads: config['threads']
    params:
        mem_mb=60000,
        out_prefix='intermediate/rnaseq/rsem/{sample}_rsem',
        in_prefix=rsem_prefix,
    shell:
        '{CONDA} convert-sam-for-rsem '
        '-p {threads} '
        '--memory-per-thread {params.mem_mb}M '
        '{input.bam} {params.out_prefix} && '
        'rsem-calculate-expression '
        '-p {threads} '
        '--bam ' # type of input file
        '--paired-end '
        '--estimate-rspd '
        '--calc-ci '
        '--append-names '
#        '--forward-prob 0 ' # for stranded single end
        '--seed 12345 ' # make it deterministic
        '--ci-memory {params.mem_mb} '
        '--no-bam-output '
        '{output.bam} '
        '{params.in_prefix} '
        '{params.out_prefix}; '
        'if [ -d {params.out_prefix}.stats ]; then '
        'rm -rf {params.out_prefix}.stats; '
        'fi;'
# vdjer
# rule vdjer:
#     input: rules.samtools_sort.output
#     output: 'intermediate/bcell/vdjer/0_vdjer/{sample}'

#aggregate_rsem = srcdir('../scripts/aggregate_counts.R')


rule aggregate_rsem:
    input: expand(rules.rsem.output.final, sample=samples)
    output: 'output/rsem/aggregate_tpm.txt'
    run:
        columns = {'CELL_ID': samples}
        merge_input(input, output,
                    add_columns=columns,
                    merge_headers=True)
        # '{CONDA} Rscript '
        # '{aggregate_script} '
        # '--infiles {input} '
        # '--sample-ids {params.sample_ids} '
        # '--outfile {output}'


# now vdjpuzzle
rule intersect_bed:
    input:
        bam=rules.star.output.genome,
        bed=rules.extract_receptors.output
    output: 'intermediate/vdjpuzzle/0_intersect/{sample}_chain-{chain}_intersect.bam'
    shell:
        '{CONDA} intersectBed '
        '-wa -abam {input.bam} '
        '-b {input.bed} '
        '> {output}'


# find all read IDs overlapping
rule overlapping_read_ids:
    input: rules.intersect_bed.output
    output: 'intermediate/vdjpuzzle/1_overlapping_reads/{sample}_chain-{chain}_ids.txt'
    shell:
        '{CONDA} samtools view {input} | cut -f 1 | '
        'sort | uniq > {output}' # convert to read IDs only

# find fastq entries containing overlapping read IDs from trimmed fastq files
rule overlapping_reads:
    input:
        reads=lambda wc: _star_input(wc, read='read_one') if wc.num == '1' else _star_input(wc, read='read_two'),
        ids=rules.overlapping_read_ids.output
    output: 'intermediate/vdjpuzzle/1_overlapping_reads/{sample}_chain-{chain}_R{num}.fastq'
    shell:
        'zcat {input.reads} | '
        'grep -f {input.ids} -A 3 -F | '
        'egrep -v "^\-\-$" ' # get rid of pesky -- lines which appear for some reason
        '> {output} || touch {output}'


rule trinity_first:
    input: expand('intermediate/vdjpuzzle/1_overlapping_reads/{{sample}}_chain-{{chain}}_R{num}.fastq', num=[1,2])
    output: 'intermediate/vdjpuzzle/2_trinity_first/{sample}_chain-{chain}.fa'
    params:
        temp_dir='intermediate/vdjpuzzle/2_trinity_first/{sample}_chain-{chain}_trinity'
    shell:
        '''
        mkdir -p {params.temp_dir}
        {CONDA} Trinity --left {input[0]} --right {input[1]} --seqType fq --max_memory 10G --output {params.temp_dir} || true
        if [ -f {params.temp_dir}/Trinity.fasta ]; then
            mv {params.temp_dir}/Trinity.fasta {output}
        else
            touch {output}
        fi
        rm -rf {params.temp_dir}
        '''


# just to see what the repertoire is after the initial reconstruction
rule migmap_first:
    input: rules.trinity_first.output
    output: 'intermediate/vdjpuzzle/3_migmap_first/{sample}_chain-{chain}_migmap.txt'
    threads: config['threads']
    shell:
        '{CONDA} migmap '
        '-R {wildcards.chain} '
        '-S {config[species]} '
        '--data-dir=$CONDA_PREFIX/share/igblast '
        '-p {threads} '
        '{input} {output}'


rule migmap_by_read:
    input: rules.trinity_first.output
    output:
        tmp='intermediate/vdjpuzzle/4_migmap_by_read/{sample}_chain-{chain}_tmp.txt',
        final='intermediate/vdjpuzzle/4_migmap_by_read/{sample}_chain-{chain}_matching_reads.fa'
    threads: config['threads']
    shell:
        '{CONDA} migmap '
        '-R {wildcards.chain} '
        '-S {config[species]} '
        '--data-dir=$CONDA_PREFIX/share/igblast '
        '-p {threads} '
        '--by-read '
        '{input} {output.tmp}; ' #
        '''
        if [[ $(wc -l < {output.tmp}) == "1" ]]; then
            touch {output.final}
        else
            samtools faidx {input};
            tail -n+2 {output.tmp} | cut -f 1 -d " " | cut -c 2- | xargs -n 1 samtools faidx {input} > {output.final}
        fi
        '''


rule combine_matching_reads:
    # combine across samples, keep chains separate
    input: expand(str(rules.migmap_by_read.output.final).replace('{chain}', '{{chain}}'), sample=samples)
    output: 'intermediate/vdjpuzzle/5_matching_reads/all_sequences_{chain}.fa'
    shell:
        'cat {input} > {output}'


rule bowtie_index_chain:
    input: rules.combine_matching_reads.output
    # use a dummy file for output since bowtie2-build crashes if the input file is empty. This can
    #  happen if for example if there are no T-cells with delta and gamma chains.
    output: 'intermediate/vdjpuzzle/6_bowtie_index/{chain}.btdone'
    params:
        prefix='intermediate/vdjpuzzle/6_bowtie_index/{chain}'
    shell:
        '''
        if [[ -s {input} ]]; then
            {CONDA} bowtie2-build -f {input} {params.prefix} && touch {output}
        else
            touch {output}
        fi
        '''


rule vdjpuzzle_bowtie_map:
    input:
        bowtie=rules.bowtie_index_chain.output,
        r1=lambda wc: _star_input(wc, read='read_one'),
        r2=lambda wc: _star_input(wc, read='read_two')
    output:
        concordant_r1='intermediate/vdjpuzzle/7_bowtie_map/{sample}_chain-{chain}_concordant_R1.fastq',
        concordant_r2='intermediate/vdjpuzzle/7_bowtie_map/{sample}_chain-{chain}_concordant_R2.fastq',
        bam=temp('intermediate/vdjpuzzle/7_bowtie_map/{sample}_chain-{chain}_mapped.bam')
    params:
        bowtie_prefix='intermediate/vdjpuzzle/6_bowtie_index/{chain}',
        concordant_prefix='intermediate/vdjpuzzle/7_bowtie_map/{sample}_chain-{chain}_concordant_R%.fastq'
    threads: config['threads']
    shell:
        '''
        if [ -e {params.bowtie_prefix}.1.bt2 ]; then
        '''
            '{CONDA} bowtie2 '
            '--no-unal '
            '-p {threads} '
            '-k 1 '
            '--np 0 '
            '--rdg 1,1 '
            '--rfg 1,1 '
            '-x {params.bowtie_prefix} '
            '-1 {input.r1} '
            '-2 {input.r2} '
            '--al-conc {params.concordant_prefix} | '
            'samtools view -@ {threads} - ' # pipe to bam
            '> {output.bam}'
        # if the bowtie index files aren't there, that means there are no cells with the chain type.
        #  Produce empty output files.
        '''
        else
            touch {output}
        fi
        '''


rule trinity_second:
    input:
        r1=rules.vdjpuzzle_bowtie_map.output.concordant_r1,
        r2=rules.vdjpuzzle_bowtie_map.output.concordant_r2,
    output: fasta='intermediate/vdjpuzzle/8_trinity_second/{sample}_chain-{chain}_trinity.fa'
    params:
        temp_dir='intermediate/vdjpuzzle/8_trinity_second/{sample}_chain-{chain}_trinity'
    shell:
        'mkdir -p {params.temp_dir}; ' #
        '{CONDA} Trinity '
        '--left {input.r1} '
        '--right {input.r2} '
        '--seqType fq '
        '--max_memory 10G '
        '--output {params.temp_dir} || true; ' #
        '''
        if [ -f {params.temp_dir}/Trinity.fasta ]; then
            mv {params.temp_dir}/Trinity.fasta {output}
        else
            touch {output}
        fi
        rm -rf {params.temp_dir}
        '''

rule migmap_second:
    input: rules.trinity_second.output
    output: 'intermediate/vdjpuzzle/9_migmap_second/{sample}_chain-{chain}_migmap.txt'
    threads: config['threads']
    shell:
        '{CONDA} migmap '
        '-R {wildcards.chain} '
        '-S {config[species]} '
        '--data-dir=$CONDA_PREFIX/share/igblast '
        '-p {threads} '
        '{input} {output}'

rule corrected_sequence:
    input:
        r1=rules.vdjpuzzle_bowtie_map.output.concordant_r1,
        r2=rules.vdjpuzzle_bowtie_map.output.concordant_r2,
        trinity=rules.trinity_second.output.fasta
    output:
        fasta='intermediate/vdjpuzzle/10_corrected_sequence/{sample}_{chain}_corrected.fasta'
    params:
        reference='intermediate/vdjpuzzle/10_corrected_sequence/reference_{sample}_{chain}.fasta',
        alignment='intermediate/vdjpuzzle/10_corrected_sequence/alignment_{sample}_{chain}.bam'
    log: log='intermediate/vdjpuzzle/10_corrected_sequence/{sample}_{chain}.log'
    threads: config['threads']
    # This rule generates may temporary files that would be painful to remove
    #  individually. The shadow directive causes the rule to be executed in a
    #  temporary directory which is deleted afterwards.
    shadow: "full"
    run:
        from scripts.mpileup2cons import mpileup2cons

        with open(log.log, "w") as log_file:
            # Create a dictionary of all the sequences in the fasta file so we can
            #  grab individual sequences later on.
            all_sequences = SeqIO.index(input.trinity, "fasta")

            # Make sure output file always exists even if we don't write anything in it
            open(output.fasta, "w").close()

            # Iterate over sequences that pass migmap quality filter_counts.
            #  First tab delimited entry from migmap is the sequence name.
	        #  We use the '|| true' because egrep returns an error for empty
	        #  files, which we don't want.
            accepted_sequences_cmd = (
                '{CONDA} migmap '
                '-R {wildcards.chain} '
                '-S {config[species]} '
                '--data-dir=$CONDA_PREFIX/share/igblast '
                '-p {threads} '
                '--by-read '
                '{input.trinity} - | cut -f1 | '
                'sed -n -e "s/^>\\(.*\\)$/\\1/p"' # Get sequence name without the '>'
                )
            for index, seq_name in enumerate(shell(accepted_sequences_cmd, iterable=True)):
                seq_name_prefix = seq_name.split(' ', 1)[0]
                old_seq_record = all_sequences[seq_name_prefix]
                log_file.write("Processing sequence {0}: {1}\n".format(index, seq_name_prefix))
                log_file.write(">{0}\n{1}\n".format(seq_name, old_seq_record.seq))
                # Write sequence to file
                SeqIO.write(old_seq_record, params.reference, "fasta")
                # Index the sequence
                shell('{CONDA} samtools faidx {params.reference}')
                shell('{CONDA} bwa index {params.reference}')
                # Align reads to sequence
                shell('{CONDA} bwa mem {params.reference} {input.r1} {input.r2} | '
                    'samtools view -b | samtools sort -o {params.alignment}')
                shell('{CONDA} samtools index {params.alignment}')
                # Modify reference with the concensus of any SNPs or INDELs in the reads
                changes_csv_iter = shell( \
                    '{CONDA} samtools mpileup -uf {params.reference} {params.alignment} | '
                    'bcftools call -c --skip-variants indels', iterable=True)
                log_file.write("Executing mpileup2cons...\n")
                new_seq_record = mpileup2cons(changes_csv_iter, old_seq_record, log_file)
                log_file.write("New sequence:\n{0}\n".format(new_seq_record.seq))
                # Append the revised reference sequence to the new fasta file
                with open(output.fasta, "a") as f:
                    SeqIO.write(new_seq_record, f, "fasta")

rule migmap_second_corrected:
    input: rules.corrected_sequence.output.fasta
    output: 'intermediate/vdjpuzzle/11_migmap_second_corrected/{sample}_chain-{chain}_migmap_corrected.txt',
    threads: config['threads']
    shell:
        '{CONDA} migmap '
        '-R {wildcards.chain} '
        '-S {config[species]} '
        '--data-dir=$CONDA_PREFIX/share/igblast '
        '-p {threads} '
        '{input} {output}'

def _merge_vdjpuzzle(wildcards):
    # do this to ensure order of input files
    return expand(rules.migmap_second_corrected.output,
                  zip,
                  sample=sorted(samples*len(relevant_chains)),
                  chain=relevant_chains*len(samples))

rule merge_vdjpuzzle:
    input: _merge_vdjpuzzle
    output: 'output/vdjpuzzle/vdjpuzzle_merged.txt'
    run:
        columns = {'CHAIN': relevant_chains*len(samples),
                   'CELL_ID': sorted(samples*len(relevant_chains))}
        merge_input(input, output,
                    add_columns=columns,
                    merge_headers=True)

# mixcr - separate dag
# c.f. https://mixcr.readthedocs.io/en/latest/rnaseq.html
rule mixcr_align:
    input:
        r1=rules.trimmomatic_pe.output.r1,
        r2=rules.trimmomatic_pe.output.r2
    output: 'intermediate/mixcr/1_alignments/{sample}_alignments.vdjca'
    threads: config['threads']
    params:
        species='hsa' if config['species'] == 'human' else 'IMPLEMENT_ME',
        chains=','.join(relevant_chains)
    shell:
        '{CONDA} mixcr align '
        '-f '
        '--threads {threads} '
        '-c {params.chains} '
        '-p rna-seq '
        '-s {params.species} '
        '-OallowPartialAlignments=true '
        '{input} {output}'


rule mixcr_assembly:
    input: rules.mixcr_align.output
    output:
        first_round=temp('intermediate/mixcr/2_assembly/{sample}_rescued_1.vdjca'),
        second_round='intermediate/mixcr/2_assembly/{sample}_rescued_2.vdjca'
    shell:
        '''
        {CONDA} mixcr assemblePartial -f {input} {output.first_round}
        {CONDA} mixcr assemblePartial -f {output.first_round} {output.second_round}
        '''


rule mixcr_clonotypes:
    input: rules.mixcr_assembly.output.second_round
    output: 'intermediate/mixcr/3_clonotypes/{sample}_clones.clns'
    shell:
        '{CONDA} mixcr assemble -f {input} {output}'


rule mixcr_export:
    input: rules.mixcr_clonotypes.output
    output: 'output/mixcr/{sample}_clones.txt'
    shell:
        '{CONDA} mixcr exportClones -f -s {input} {output}'


rule merge_mixcr:
    input: expand(rules.mixcr_export.output, sample=samples)
    output: 'output/mixcr_merged.txt'
    run:
        columns = {'CELL_ID': samples}
        merge_input(input, output,
                    add_columns=columns,
                    merge_headers=True)

filter_mixcr_script = srcdir('../scripts/filter_mixcr_clones.R')

rule filter_mixcr:
    input: rules.merge_mixcr.output
    output: 'output/mixcr_merged_filtered.txt'
    shell:
        '{CONDA} Rscript {filter_mixcr_script} '
        '--infile {input} '
        '--outfile {output} '
        '--rename-cols'

rule feature_counts:
    input:
        bam=_bam_input,
        gtf=_starindex_annotation
    output: 'intermediate/rnaseq/3_counts/{sample}_counts.txt'
    threads: config['threads']
    shell:
        '{CONDA} featureCounts '
        '-T {threads} '
        '-t exon ' # default
        '-p ' # paired end
        #'-B ' # only count read pairs where both ends align
        #'-P ' # check validity of paired-end distance; 50-600
        '-C ' # do not count chimeric reads (different chromosomes)
        '-g gene_id ' # default
        #'-M ' # allow multi-mapping reads
        #'--fraction ' # assign fractional counts to features
        '-a {input.gtf} '
        '-o {output} '
        '{input.bam}'

aggregate_script = srcdir('../scripts/aggregate_counts.R')

rule aggregate_counts:
    input: expand(rules.feature_counts.output, sample=samples)
    output: 'intermediate/rnaseq/4_aggregate/aggregate_counts.txt'
    params:
        sample_ids=' '.join(samples) # pass list of sample ids to become colnames
    shell:
        '{CONDA} Rscript '
        '{aggregate_script} '
        '--infiles {input} '
        '--sample-ids {params.sample_ids} '
        '--outfile {output}'

# rule filter_counts:
#     input: rules.aggregate_counts.output
#     output: 'intermediate/rnaseq/5_filter/filtered_counts.txt'
#     shell:
#         '{CONDA} Rscript '


rule basic:
    input:
        read_one=lambda wc: _star_input(wc, read='read_one'),
        read_two=lambda wc: _star_input(wc, read='read_two')
    params:
        basic=config['basic_path']
    output: 'intermediate/bcell/basic/0_basic/{sample}.fasta'
    shell:
        '{CONDA} python {params.basic} '
        '-b $(dirname `which bowtie2`) '
        '-PE_1 {input.read_one} '
        '-PE_2 {input.read_two} '
        '-n {wildcards.sample}; '
        'mv {wildcards.sample}.fasta {output}; '
        '''
        for CREGION in "ighc" "ighv" "iglc" "iglv" "igkc" "igkv"; do
            if [[ -f {wildcards.sample}.$CREGION ]]; then
                mv {wildcards.sample}.$CREGION "$(dirname {output})"
            fi
        done
        '''


rule bcr_basic_migmap:
    input: rules.basic.output
    output: 'intermediate/bcell/basic/1_migmap/{sample}_migmap.txt'
    threads: config['threads']
    shell:
        '{CONDA} migmap '
        '--allow-incomplete '
        '--all-alleles '
        '--allow-no-cdr3 '
        '--allow-noncoding '
        '--allow-noncanonical '
        '--details fr1nt,cdr1nt,cdr1aa,fr2nt,cdr2nt,cdr2aa,fr3nt,fr4nt,contignt,contigaa '
        '--by-read '
        '-R IGH,IGK,IGL '
        '-S human '
        '--data-dir=$CONDA_PREFIX/share/igblast '
        '-p {threads} '
        '{input} {output}'

# can only do srcdir in the Snakefile, not in the rules
deparse_migmap = srcdir('../scripts/deparse_and_merge_migmap.R')

rule merge_bcr_migmap:
    input: expand(rules.bcr_basic_migmap.output, sample=samples)
    output: 'intermediate/bcell/basic/2_merged/merged.txt'
    shell:
        '{CONDA} Rscript '
        '{deparse_migmap} '
        '--infiles {input} '
        '--outfile {output} '
        '--deparse-basic'

# define receptor_targets
if config['receptor_type'] == 'B':
    receptor_targets = rules.merge_bcr_migmap.output
elif config['receptor_type'] == 'T':
    receptor_targets = [] # TODO implement T cell receptor calling
elif config['receptor_type'] is None:
    receptor_targets = []
else:
    raise Exception('config["receptor_type"] must be either B or T.')

# define other targets
vdjpuzzle_targets = [rules.merge_vdjpuzzle.output]
rnaseq_targets = [rules.aggregate_counts.output, rules.aggregate_rsem.output]
mixcr_targets = [rules.filter_mixcr.output]

rule indexes:
    input:
        star=starindex_output,
        rsem=rsem_reference

rule receptors:
    input: receptor_targets

rule vdjpuzzle:
    input: vdjpuzzle_targets

rule rnaseq:
    input: rnaseq_targets

rule mixcr:
    input: mixcr_targets

rule all:
    input:
        rnaseq=rnaseq_targets,
        receptor=receptor_targets,
        vdjpuzzle=vdjpuzzle_targets

# clean up log directories so that we don't exceed inode quotas
def clean_log_dir():
    shell('''
          sleep 5;
          for SUFFIX in err out; do
              for FILE in `find ./log/run/{config[run_number]} -name "rule*.$SUFFIX"`; do
                  echo "$FILE" >> log/run/{config[run_number]}/run_rules.$SUFFIX;
                  cat $FILE >> log/run/{config[run_number]}/run_rules.$SUFFIX;
                  rm -f $FILE;
              done;
          done;
          ''')


onsuccess:
    clean_log_dir()

onerror:
    clean_log_dir()
